@extends('layouts.loginHead')

@section('title')
    S'identifier
@endsection

<body class="green lighten-1">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->



<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12 center">
                    <a href="{{ url('/') }}"><img src="{{asset('img/login-logo.png')}}" alt="" class="circle responsive-img valign profile-image-login"></a>
                    <p class="center login-form-text">ICACOOP</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="البريد الإلكتروني">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="email" type="text" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

                    <label for="username" class="center-align">Email</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="كلمه السري">
                <i class="mdi-communication-vpn-key prefix"></i>
                <input id="password" type="password">

                @if ($errors->has('password'))
                    <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif

                <label for="password">Mot de passe</label>
            </div>
    </div>
    <div class="row">
        <div class="input-field col s12 m12 l12  login-text tooltipped" data-position="right" data-delay="50" data-tooltip="احفظ معلوماتي">
            <input type="checkbox" id="remember-me" name="remember"/>
            <label for="remember-me">Mémoriser mes informations</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <button type="submit" class="btn green lighten-1 waves-effect waves-light col s12 tooltipped" data-position="right" data-delay="50" data-tooltip="تسجيل الدخول" >Connexion</button>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s4 m4 l4 tooltipped " data-position="bottom" data-delay="50" data-tooltip="سجل الان" >
            <p class="margin medium-small"><a href="page-register.html">Inscrire!</a></p>
        </div>
        <div class="input-field col s8 m8 l8 tooltipped" data-position="bottom" data-delay="50" data-tooltip="هل نسيت كلمة المرور ؟">
            <p class="margin right-align medium-small"><a href="{{ url('/password/reset') }}">Mot de passe oublié?</a></p>
        </div>
    </div>

    </form>
</div>
</div>



<!-- ================================================
  Scripts
  ================================================ -->

@extends('layouts.loginFooter')

</body>

</html>