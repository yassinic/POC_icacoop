@extends('layouts.loginHead')

@section('title')
    Rejoignez-nous
@endsection

<body class="green lighten-1">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->



<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="row">
                <div class="input-field col s12 center">
                    <h4>ICACOOP</h4>
                    <p class="center">Rejoignez-nous aujourd'hui!</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('name') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="اسم">
                    <i class="mdi-social-person-outline prefix"></i>
                    <input id="name" type="text" value="{{ old('name') }}">
                    <label for="name" class="center-align">Nom</label>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="البريد الإلكتروني">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="email" type="email" value="{{ old('email') }}">
                    <label for="email" class="center-align">Email</label>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="كلمة السر">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password">
                    <label for="password">Mot de passe</label>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }} tooltipped" data-position="right" data-delay="50" data-tooltip="كرر كلمة المرور">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password-again" type="password">
                    <label for="password-again">Répéter le mot de passe</label>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif

                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 tooltipped" data-position="right" data-delay="50" data-tooltip="انضم إلينا">
                    <button type="submit" class="btn waves-effect waves-light col s12">Rejoignez-nous</button>
                </div>
                <div class="input-field col s12 tooltipped" data-position="bottom" data-delay="50" data-tooltip="تسجيل الدخول">
                    <p class="margin center medium-small sign-up">Vous avez déjà un compte? <a href="{{url('/login')}}">S'identifier</a></p>
                </div>
            </div>
        </form>
    </div>
</div>



<!-- ================================================
  Scripts
  ================================================ -->

@extends('layouts.loginFooter')

</body>